/**
 * Created by tcorbu1 on 10.03.2017.
 */
'use strict';
const path = require('path');
// var webpack = require('webpack');

const OUTPUT_PATH = path.join(__dirname, 'dist');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCSS = new ExtractTextPlugin('[name].css');

module.exports = [
    {
        entry: function () {
            return ['./index.js']
        },
        output: {
            path: OUTPUT_PATH,
            filename: 'index.js'
        },
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    loader: extractCSS.extract(
                        {
                            fallback: 'style-loader',
                            use: ['css-loader', 'sass-loader']
                        })
                }
            ]
        },
        plugins: [
            extractCSS
        ]
    }
];